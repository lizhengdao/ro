#################################

Punteggio: 932
Peso zaino: 9.98

Lo zaino contiene:
cioccolato: 2
succo: 2
birra: 6
panino: 40
acqua: 0
biscotti: 2

#################################

ity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 punteggio_totale
                                 932                             
     2 peso_zaino               9.98                          10 

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 take[cioccolato]
                    *              2             2               
     2 take[succo]  *              2             2               
     3 take[birra]  *              6             6               
     4 take[panino] *             40            10               
     5 take[acqua]  *              0             0               
     6 take[biscotti]
                    *              2             2               

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output

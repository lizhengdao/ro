/* Parametri */
set Modelli;
param profitto{t in Modelli};
param richiesta{t in Modelli};
param nA{t in Modelli};
param nB{t in Modelli};
param nL{t in Modelli};

param QA;
param QB;
param QL;

var take{i in Modelli}, integer, >= richiesta[i];

/* Vincoli */

sumA : sum{m in Modelli} take[m] * nA[m] <= QA;
sumB : sum{m in Modelli} take[m] * nB[m] <= QB;
sumL : sum{m in Modelli} take[m] * nL[m] <= QL;

/* Funzione Obiettivo */
maximize max_profitto: sum{m in Modelli} take[m] * profitto[m];
 
solve;

# Report
printf '#################################\n\n';
printf "Produzione ottimale:\n";
printf "Profitto: %i\n", max_profitto;
printf "Pezzi A usati: %i\n", sumA;
printf "Pezzi B usati: %i\n", sumB;
printf "Forza Lavoro: %i\n", sumL;

printf "\n";
printf {m in Modelli} "Modello %i: %i pezzi\n", m, take[m];
printf "\n";
printf '#################################\n\n';
end;

#  Un problema meno semplice di pianificazione della produzione.
## es 1.4.3
#### Soluzione GNU Linear Programming Kit
base
```bash
$ glpsol -d base.dat -m base.mod -o base.sol -y base.sol
$ vim base.sol
```
ottimizzato
```bash
$ glpsol -d ottimizzato.dat -m ottimizzato.mod -o ottimizzato.sol -y ottimizzato.sol
$ vim ottimizzato.sol
```
#### Problema
Un’azienda produce tre modelli 1, 2 e 3 di un certo prodotto. Ciascun modello richiede
due tipi di materiali grezzi (A e B) di cui sono disponibili rispettivamente 4000 e 6000
unità. In particolare, per produrre una unità del modello 1 sono necessarie 2 unità di
A e 4 unità di B; per una unità del modello 2 sono necessarie 3 unità di A e 2 unità
di B; per una unità del modello 3 sono necessarie 5 unità di A e 7 di B. Il modello
1 richiede, per ogni unità prodotta, il doppio di forza lavoro rispetto al modello 2 e il
triplo rispetto al modello 3. La forza lavoro presente in azienda è in grado di produrre al
massimo l’equivalente di 700 unità/giorno del modello 1. Il settore marketing dell’azienda
ha reso noto che la domanda minima per ciascun modello è rispettivamente di 200, 200 e
150 unità. Il profitto unitario di ogni modello è di 30, 20 e 50 euro, rispettivamente.
Formulare il programma lineare per pianificare la produzione giornaliera massimizzando
il profitto.
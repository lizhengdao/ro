set I;
param s{I};

var t{i in I}, integer, >= 0;

var e{i in I}, integer;

minimize errori:
    sum {i in I} e[i];
    
oi1: t[1] + 5 <= t[2];
oi2: t[2] + 7 <= t[3];
oi3: t[3] + 4 <= t[4];
oi4: t[4] + 7 <= t[5];

errore1: e[1] >= t[1] + 5 - 122;
errore3: e[1] >= -(t[1] + 5 - 122);
errore4: e[2] >= t[2] + 7 - 128;
errore5: e[2] >= -(t[2] + 7 - 128);
errore6: e[3] >= t[3] + 4 - 132;
errore7: e[3] >= -(t[3] + 4 - 132);
errore8: e[4] >= t[4] + 7 - 142;
errore9: e[4] >= -(t[4] + 7 - 142);
errore10: e[5] >= t[5] + 10 - 147;
errore11: e[5] >= -(t[5] + 10 - 147);
 

solve; 


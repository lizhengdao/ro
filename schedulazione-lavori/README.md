# Il problema del personale di un motel.
## es 1.4.5
#### Soluzione GNU Linear Programming Kit
parte a
```bash
$ glpsol -d base.dat -m base.mod -o base.sol
$ vim base.sol
```
#### Problema
Un motel autostradale, dovendo garantire un servizio continuato 24 ore su 24, ha bisogno di un numero minimo di inservienti
per ogni ora del giorno secondo la seguente tabella.
| Fascia Oraria | Numero min |
|---------------|-----------:|
| 02-06         | 10         |
| 06-10         | 8          |
| 10-14         | 10         |
| 14-18         | 7          |
| 18-22         | 12         |
| 22-02         | 4          |

Ciascun inserviente lavora 8 ore consecutive al giorno.
Formulare il modello di Programmazione Lineare per garantire la presenza richiesta utilizzando il minor numero possibile di inservienti.

